local Ip2region = require "Ip2region";
arg[1]="/Users/dean/Downloads/ip2region/data/ip2region.db";
algorithm = "memory";

local ip2region = Ip2region.new(arg[1]);

local line = "113.245.74.60";

if ( ip2region.ip2long(line) == nil ) then
    print("Invalid ip address=", line);
else
    local data;
    local s_time = os.clock();
    if ( algorithm == "btree" ) then
        data = ip2region:btreeSearch(line);
    elseif ( algorithm == "binary" ) then
        data = ip2region:binarySearch(line);
    elseif ( algorithm == "memory" ) then
        data = ip2region:memorySearch(line);
    end

    local cost_time = (os.clock() - s_time) * 1000; -- to millseconds
    if ( data == nil ) then
        print("Failed for ip=", line, " is it a valid ip address ?");
    else
        print(string.format("%u|%s in %5f millseconds\n", data.city_id, data.region, cost_time));
    end
end
